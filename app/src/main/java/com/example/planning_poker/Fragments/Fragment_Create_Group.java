package com.example.planning_poker.Fragments;

import android.os.Bundle;
import android.widget.Button;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.planning_poker.Classes.Group;
import com.example.planning_poker.R;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;

public class Fragment_Create_Group extends Fragment {

    DatabaseReference myRef;

    Group group;
    Button submit_button;
    EditText group_name,group_id,topic_name,timer;
    CheckBox checkBox;
    public Fragment_Create_Group() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment__create__group, container, false);

        myRef = FirebaseDatabase.getInstance().getReference("Groups");

        submit_button = (Button) v.findViewById(R.id.group_submit);
        checkBox = (CheckBox) v.findViewById(R.id.checkBox);
        group_name = (EditText) v.findViewById(R.id.group_name);
        group_id = (EditText) v.findViewById(R.id.group_id);
        topic_name = (EditText) v.findViewById(R.id.topic_name);
        timer = (EditText) v.findViewById(R.id.timer);



        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "onClick: started ", Toast.LENGTH_LONG).show();
                addGroup();
            }
        });

        return v;
    }

    private void addGroup(){

        if (group_name.getText().toString().isEmpty()){
            group_name.setError("Please enter a name!");
            group_name.requestFocus();
        }
        else if (group_id.getText().toString().isEmpty()){
            group_id.setError("Please enter an ID!");
            group_id.requestFocus();
        }

        else if (topic_name.getText().toString().isEmpty()){
            topic_name.setError("Please enter a Topic Name!");
            topic_name.requestFocus();
        }
        else {
            String name = group_name.getText().toString().trim();
            int id = Integer.parseInt(group_id.getText().toString());
            String topic_name_text = topic_name.getText().toString().trim();

            group = new Group(name, id);
            if (checkBox.isChecked()){
                group.setTimer(Integer.parseInt(timer.getText().toString()));
            }
            group.topics.add(topic_name_text);

            myRef.child(String.valueOf(id)).setValue(group);
            Toast.makeText(getContext(), "Done ", Toast.LENGTH_LONG).show();


            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragmentLayout, new Fragment_Scrum_Master()).addToBackStack("fragmentStack").commit();

        }

    }

}
