package com.example.planning_poker.Fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.planning_poker.Classes.Group;
import com.example.planning_poker.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Fragment_Edit_Group extends Fragment {

    DatabaseReference myRef;

    Group group;
    Button submit_button,show_button;
    EditText group_name,group_id,topic_name,timer,targetID;
    CheckBox checkBox;

    public Fragment_Edit_Group() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fragment__edit__group, container, false);

        targetID = (EditText) v.findViewById(R.id.edit_group_id);
        show_button = (Button) v.findViewById(R.id.show_button);
        submit_button = (Button) v.findViewById(R.id.editGroup_submit);
        checkBox = (CheckBox) v.findViewById(R.id.editCheckBox);
        group_name = (EditText) v.findViewById(R.id.editGroup_name);
        group_id = (EditText) v.findViewById(R.id.editGroup_id2);
        topic_name = (EditText) v.findViewById(R.id.editTopic_name);
        timer = (EditText) v.findViewById(R.id.editTimer);

        show_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = Integer.valueOf(targetID.getText().toString());
                String sid = targetID.getText().toString();
                myRef = FirebaseDatabase.getInstance().getReference().child("Groups").child(sid);
                myRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String sgroup_name = dataSnapshot.child("name").getValue().toString();
                        String stimer = dataSnapshot.child("timer").getValue().toString();
                        String stopic_name = dataSnapshot.child("topics").child("0").getValue().toString();
                        String sid = dataSnapshot.child("id").getValue().toString();
                        group_name.setText(sgroup_name);
                        timer.setText(stimer);
                        topic_name.setText(stopic_name);
                        group_id.setText(sid);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });

        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modifyGroup();
            }
        });
        return v;
    }

    private void modifyGroup(){

        if (group_name.getText().toString().isEmpty()){
            group_name.setError("Please enter a name!");
            group_name.requestFocus();
        }
        else if (group_id.getText().toString().isEmpty()){
            group_id.setError("Please enter an ID!");
            group_id.requestFocus();
        }

        else if (topic_name.getText().toString().isEmpty()){
            topic_name.setError("Please enter a Topic Name!");
            topic_name.requestFocus();
        }
        else {
            String name = group_name.getText().toString().trim();
            int id = Integer.parseInt(group_id.getText().toString());
            String topic_name_text = topic_name.getText().toString().trim();

            group = new Group(name, id);
            if (checkBox.isChecked()){
                group.setTimer(Integer.parseInt(timer.getText().toString()));
            }
            group.topics.add(topic_name_text);

            myRef.setValue(group);
            Toast.makeText(getContext(), "Done ", Toast.LENGTH_LONG).show();


            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragmentLayout, new Fragment_Scrum_Master()).addToBackStack("fragmentStack").commit();

        }

    }

}
