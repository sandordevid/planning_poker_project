package com.example.planning_poker.Fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.planning_poker.R;
import com.example.planning_poker.Adapters.RecyclerViewAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class Fragment_Scrum extends Fragment {

    DatabaseReference myRef;
    ValueEventListener listener;
    String group_name,group_id;
    private ArrayList<String> mTexts = new ArrayList<>();
    public int[] dummies = {1,2,3,4,5,6,7,8,9,10};
    public Fragment_Scrum() {
        //empty constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_fragment__scrum, container, false);
        Log.d(TAG, "onCreateView: started.");

        retrieveData();
        initRecyclerView(v);

        return v;
    }


    private void retrieveData(){
        myRef = FirebaseDatabase.getInstance().getReference();
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                group_id = dataSnapshot.child("Active").getValue().toString();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void initRecyclerView(View v){
        Log.d(TAG, "initRecyclerView: init recyclerView");

        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.recycler);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(dummies,getContext(),group_id);
        recyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
    }
}
