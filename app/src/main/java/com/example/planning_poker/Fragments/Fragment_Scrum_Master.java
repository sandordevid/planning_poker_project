package com.example.planning_poker.Fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.example.planning_poker.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class Fragment_Scrum_Master extends Fragment {

    DatabaseReference databaseReference;
    ValueEventListener listener;
    ArrayAdapter<String> adapter;
    ArrayList<String> spinnerDataList;
    Spinner groupSpinner;
    Button create_button,edit_button,activate_button;

    String selectedID,selectedName;
    public Fragment_Scrum_Master() {
        //empty constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment__scrum__master, container, false);

        groupSpinner = v.findViewById(R.id.group_spinner);
        databaseReference = FirebaseDatabase.getInstance().getReference("Groups");

        spinnerDataList = new ArrayList<>();
        adapter = new ArrayAdapter<String>(v.getContext(),R.layout.support_simple_spinner_dropdown_item,spinnerDataList);
        groupSpinner.setAdapter(adapter);
        retrieveData();
        Log.d(TAG, "onCreateView: started.");
        create_button = v.findViewById(R.id.create_group);
        create_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragmentLayout, new Fragment_Create_Group())
                        .commit();
            }
        });

        edit_button = v.findViewById(R.id.edit_group);
        edit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragmentLayout, new Fragment_Edit_Group())
                        .commit();
            }
        });

        activate_button = v.findViewById(R.id.activate_button);
        activate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               selectedName = groupSpinner.getSelectedItem().toString();
                listener = databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot item:dataSnapshot.getChildren()){
                            if (selectedName == item.child("name").getValue().toString()){
                                selectedID = item.child("id").getValue().toString();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
               databaseReference.getRoot().child("Active").setValue(selectedID);
               //Nem tudom miért,de kétszer kell megnyomni az active buttont,hogy a rendes értéket mentse el az adatbázisba.
            }
        });

        return v;
    }

    public void retrieveData(){
        listener = databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot item:dataSnapshot.getChildren()){
                    spinnerDataList.add(item.child("name").getValue().toString());
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
