package com.example.planning_poker.Classes;

import java.util.ArrayList;

public class Group {
    private String name;
    private int id;
    private int timer;
    public ArrayList<String> topics;
    protected ArrayList<User> users;

    public int getTimer() {
        return timer;
    }

    public void setTimer(int timer) {
        this.timer = timer;
    }

    public ArrayList<String> getTopics() {
        return topics;
    }

    public void setTopics(ArrayList<String> topics) {
        this.topics = topics;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    public Group(String name, int id) {
        this.name = name;
        this.id = id;
        topics = new ArrayList<String>();
        users = new ArrayList<User>();
        this.timer = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
