package com.example.planning_poker.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.planning_poker.R;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder>{

    private int[] dummies;
    private Context mContext;
    private String name;


    public RecyclerViewAdapter(int[] dummies, Context mContext,String name) {
        this.dummies = dummies;
        this.mContext = mContext;
        this.name = name;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem,parent,false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        int dummy_id = dummies[position];
        //holder.mQuestion.setText("test");
        holder.mItemText.setText("Select a score: " );
        ArrayAdapter adapter = ArrayAdapter.createFromResource(mContext, R.array.scores,R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        holder.spinner.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return dummies.length;
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        TextView mItemText,mQuestion;
        Spinner spinner;

        public RecyclerViewHolder(View itemView){
            super(itemView);
            mQuestion = (TextView) itemView.findViewById(R.id.editTextDummy2);
            mItemText = (TextView) itemView.findViewById(R.id.editTextDummy);
            spinner = (Spinner) itemView.findViewById(R.id.score_spinner);

        }

    }
}
