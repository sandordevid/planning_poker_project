package com.example.planning_poker.Adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.planning_poker.R;

public class MasterRecyclerViewAdapter extends RecyclerView.Adapter<MasterRecyclerViewAdapter.RecyclerViewHolder> {

    @NonNull
    @Override
    public MasterRecyclerViewAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull MasterRecyclerViewAdapter.RecyclerViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        TextView mItemText,mQuestion;
        Spinner spinner;

        public RecyclerViewHolder(View itemView){
            super(itemView);
            mQuestion = (TextView) itemView.findViewById(R.id.editTextDummy2);
            mItemText = (TextView) itemView.findViewById(R.id.editTextDummy);
            spinner = (Spinner) itemView.findViewById(R.id.score_spinner);

        }

    }
}
