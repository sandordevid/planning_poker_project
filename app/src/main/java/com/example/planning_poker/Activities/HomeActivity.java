package com.example.planning_poker.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.planning_poker.Fragments.Fragment_Scrum;
import com.example.planning_poker.Fragments.Fragment_Scrum_Master;
import com.example.planning_poker.R;
import com.google.firebase.auth.FirebaseAuth;


public class HomeActivity extends AppCompatActivity {
    Button bLogOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Intent mIntent = getIntent();
        int master = mIntent.getIntExtra("master",0);
        //String a = String.valueOf(master);


        bLogOut = (Button)findViewById(R.id.button4);
        //bLogOut.setText(a);
        bLogOut.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FirebaseAuth.getInstance().signOut();
                Intent intToMain = new Intent(HomeActivity.this,MainActivity.class);
                startActivity(intToMain);
            }
        });

        Fragment_Scrum fragmentScrum  = new Fragment_Scrum();
        Fragment_Scrum_Master fragmentMaster = new Fragment_Scrum_Master();
        FragmentManager manager = getSupportFragmentManager();
        if(master == 1)
            manager.beginTransaction().replace(R.id.fragmentLayout,fragmentMaster,fragmentMaster.getTag()).commit();
        else
            manager.beginTransaction().replace(R.id.fragmentLayout,fragmentScrum,fragmentScrum.getTag()).commit();
    }
}
