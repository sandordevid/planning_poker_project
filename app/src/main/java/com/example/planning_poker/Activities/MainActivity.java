package com.example.planning_poker.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import android.widget.Toast;

import com.example.planning_poker.Classes.MD5;
import com.example.planning_poker.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    public static int master;
    FirebaseAuth mFirebaseAuth;
    private View decorView;
    //DatabaseReference myRef;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    EditText email_login,password_login;
    MD5 encryptor = new MD5();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        master = 0;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        email_login = (EditText) findViewById(R.id.editText);
        password_login = (EditText) findViewById(R.id.editText2);
        Button login,register;
        login = (Button) findViewById(R.id.button);
        register = (Button) findViewById(R.id.button2);
        Intent intent = getIntent();
        mFirebaseAuth = FirebaseAuth.getInstance();
/*
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();
                if (mFirebaseUser != null){
                    Toast.makeText(MainActivity.this,"You are logged in!",Toast.LENGTH_LONG).show();
                    Intent i = new Intent(MainActivity.this,HomeActivity.class);
                    startActivity(i);
                }
                else{
                    Toast.makeText(MainActivity.this,"Please Login!",Toast.LENGTH_LONG).show();
                }

            }
        };*/


        decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener(){
            @Override
            public void onSystemUiVisibilityChange(int visibility){
                if (visibility == 0)
                    decorView.setSystemUiVisibility(hideSystemBars());
            }
        });

        login.setOnClickListener(this);
        register.setOnClickListener(this);
    }
    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.button:
                final String email = email_login.getText().toString().trim();
                final String password = password_login.getText().toString().trim();
                if (email.isEmpty()){
                    email_login.setError("Please enter your email!");
                    email_login.requestFocus();
                }
                else if (password.isEmpty()){
                    password_login.setError("Please enter your password!");
                    password_login.requestFocus();
                }

               else if (email.isEmpty() && password.isEmpty()){
                    Toast.makeText(MainActivity.this,"Fields are Empty!",Toast.LENGTH_LONG).show();

                }
                else if (!(email.isEmpty() && password.isEmpty())){
                            mFirebaseAuth.signInWithEmailAndPassword(email,encryptor.MD5(password)).addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()){
                                        if (email.equals("master123@yahoo.com"))
                                            master = 1;
                                        Intent intToHome = new Intent (MainActivity.this,HomeActivity.class);
                                        intToHome.putExtra("master", master);
                                        startActivity(intToHome);
                                    }
                                    else {
                                        Toast.makeText(MainActivity.this,"Please Try Again!",Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                }
                break;
                case R.id.button2:
                    Intent intent2 = new Intent(this, RegisterActivity.class);
                    startActivity(intent2);
                    break;
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus){
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus){
            decorView.setSystemUiVisibility(hideSystemBars());
        }
    }
    private int hideSystemBars(){
        return View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
    }
/*
    protected void onStart(){
        super.onStart();
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }*/
}
