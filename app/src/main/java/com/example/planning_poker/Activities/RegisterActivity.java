package com.example.planning_poker.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import android.widget.Toast;

import com.example.planning_poker.Classes.MD5;
import com.example.planning_poker.Classes.User;
import com.example.planning_poker.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class RegisterActivity extends AppCompatActivity {
    EditText username_register,password_register,email_register;
    private FirebaseAuth mAuth;
    //DatabaseReference myRef;
    User user;
    MD5 encryptor;
    //long maxid;
    private View decorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        username_register = (EditText) findViewById(R.id.editText3);
        password_register = (EditText) findViewById(R.id.editText4);
        email_register = (EditText) findViewById(R.id.editText6);

        //user = new User();
        encryptor = new MD5();
        //myRef = database.getInstance().getReference().child("User");
        mAuth = FirebaseAuth.getInstance();
        decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener(){
            @Override
            public void onSystemUiVisibilityChange(int visibility){
                if (visibility == 0)
                    decorView.setSystemUiVisibility(hideSystemBars());
            }
        });

        Button register;
        register = (Button) findViewById(R.id.button3);

        register.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String email = email_register.getText().toString();
                String password = password_register.getText().toString();
                //user.setName(username_register.getText().toString().trim());
                //user.setEmail(email_register.getText().toString().trim());
                //user.setPassword(encryptor.MD5(password_register.getText().toString().trim()));
                //myRef.child(String.valueOf(maxid+1)).setValue(user);
                if (email.isEmpty()){
                    email_register.setError("Please enter email");
                    email_register.requestFocus();
                }
                else if (password.isEmpty()){
                    password_register.setError("Please enter your password!");
                    password_register.requestFocus();
                }
                else if (email.isEmpty() && password.isEmpty()){
                    Toast.makeText(RegisterActivity.this,"Fields are Empty!",Toast.LENGTH_LONG).show();
                }
                else if (!(email.isEmpty() && password.isEmpty())){
                    mAuth.createUserWithEmailAndPassword(email,encryptor.MD5(password)).addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                Toast.makeText(RegisterActivity.this,"Succesfully Registered",Toast.LENGTH_LONG).show();
                                startActivity(new Intent(RegisterActivity.this,MainActivity.class));
                            }else{
                                Toast.makeText(RegisterActivity.this,"Error!",Toast.LENGTH_LONG).show();

                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus){
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus){
            decorView.setSystemUiVisibility(hideSystemBars());
        }
    }
    private int hideSystemBars(){
        return View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
    }
}

